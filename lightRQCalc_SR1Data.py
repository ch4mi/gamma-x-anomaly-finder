import RQConstruct
import GammaXFinder 

import uproot
import ROOT
import awkward as ak
import numpy as np
import array as arr

def main():
    
    # READING
    readingBranches = ['Events/eventHeader./eventHeader.runID',
                       'Events/eventHeader./eventHeader.eventID',
                       'Events/pulsesTPC./pulsesTPC.pulseID',
                       'Events/pulsesTPC./pulsesTPC.bottomArea_phd',
                       'Events/pulsesTPC./pulsesTPC.topArea_phd',
                       'Events/pulsesTPC./pulsesTPC.topBottomAsymmetry',
                       'Scatters/ss./ss.s1PulseID',
                       'Scatters/ss./ss.driftTime_ns',
                       'Events/pulsesTPC./pulsesTPC.bottomCentroidX_cm',
                       'Events/pulsesTPC./pulsesTPC.bottomCentroidY_cm',
                       'Events/pulsesTPC./pulsesTPC.topCentroidX_cm',
                       'Events/pulsesTPC./pulsesTPC.topCentroidY_cm',
                       'Events/pulsesTPC./pulsesTPC.chID',
                       'Events/pulsesTPC./pulsesTPC.chPulseArea_phd',
                       'Scatters/ss.correctedS1Area_phd', 
                       'Scatters/ss./ss.correctedS2Area_phd', 
                       'Scatters/ss./ss.driftTime_ns', 
                       'Scatters/ss./ss.correctedX_cm', 
                       'Scatters/ss./ss.correctedY_cm', 
                       'Scatters/ss./ss.correctedZ_cm', 
                       'Scatters/ss./ss.nSingleScatters', 
                       'Scatters/ss./ss.s1Area_phd']

    filename = '/global/cfs/cdirs/lz/users/chami/GammaX/SR1_MSSI_Sidebands/MSSItest.root'
    
    print("\nReading in ROOT data...")
    data = GammaXFinder.readData(filename, readingBranches)
    print("\nAll branches read. Starting anomaly score calculation...")
    ifInput = GammaXFinder.preprocess(data)
    
    with open('test.npy', 'wb') as f:
        np.save(f, ifInput)
        
    # Write the ROOT file -- code from Greg
    outFile = ROOT.TFile( filename[:-5] + '_lightloaded.root', 'recreate' )
    t = ROOT.TTree( 'summary', 'summary' )

    # initialize some arrays
    evt = arr.array('i', [0])
    run = arr.array('i', [0])
    s1 = arr.array('d', [0.])
    s1r = arr.array('d', [0.])
    s2 = arr.array('d', [0.])
    x = arr.array( 'd', [0.])
    y = arr.array( 'd', [0.])
    z = arr.array( 'd', [0.])
    dt = arr.array( 'd', [0.])
    cs = arr.array( 'd', [0.])
    lmpf = arr.array( 'd', [0.])
    #tbr = arr.array( 'd', [0.])
    tba = arr.array( 'd', [0.])

    # create tree headers
    t.Branch( 'eventID', evt, 'eventID/I')
    t.Branch( 'runID', run, 'runID/I')
    t.Branch( 'S1_raw_phd', s1r, 'S1_raw_phd/D')
    t.Branch( 'S1c_phd', s1, 'S1c_phd/D' )
    t.Branch( 'S2c_phd', s2, 'S1c_phd/D' )
    t.Branch( 'drift_us', dt, 'drift_us/D')
    t.Branch( 'x_cm', x, 'X_cm/D')
    t.Branch( 'y_cm', y, 'Y_cm/D')
    t.Branch( 'z_cm', z, 'Z_cm/D')
    t.Branch( 'cluster_size_cm', cs, 'cluster_size_cm/D')
    t.Branch( 'log_MPAF', lmpf, 'log_MPAF/D')
    #t.Branch( 'TopBottomRatio', tbr, 'TBR/D')
    t.Branch( 'TopBottomAsymmetry', tba, 'TBA/D')

    nEntries = data['nSingleScatters']

    #chop the arrays to speed things up...
    outEvtID = data['eventID']
    outRunID = data['runID']
    out_S1 = data['correctedS1Area_phd']
    out_S1raw = data['s1Area_phd']
    out_S2 = data['correctedS2Area_phd']

    out_X = data['correctedX_cm']
    out_Y = data['correctedY_cm']
    out_Z = data['correctedZ_cm']
    out_drift = data['driftTime_ns']
    
    ssCS = ifInput.T[0]
    ssMPAF = ifInput.T[1]
    ssTBA = ifInput.T[2]
    
    if len(out_S1) == len(ssCS):
        print('Lightloaded vars have same length as other vars')

    for i in range(len(out_S1)):
        evt[0] = outEvtID[i]
        run[0] = outRunID[i]
        s1[0] = out_S1[i]
        s1r[0] = out_S1raw[i]
        s2[0] = out_S2[i]
        x[0] = out_X[i]
        y[0] = out_Y[i]
        z[0] = out_Z[i]
        dt[0] = out_drift[i] / 1000.
        cs[0] = ssCS[i]
        if ssMPAF[i] <= 0:
            lmpf[0] = np.nan
        else:
            lmpf[0] = np.log10( ssMPAF[i] )
        #tbr[0] = ssTBR[i]
        tba[0] = ssTBA[i]
        t.Fill()

    t.Write()
    outFile.Close()
    
    return

if __name__ == '__main__':
    main()