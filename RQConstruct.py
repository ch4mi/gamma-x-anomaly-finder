import awkward as ak
import numpy as np
from numpy import inf
import csv

def getPMTLocationDict(file = 'BPMTLocations.csv'):
    # Create a dictionary to store BPMT locations
    BPMTXLocations = {}
    BPMTYLocations = {}

    with open(file) as BPMTDataFile:
        next(BPMTDataFile) # Skip the header line
        csvReader = csv.reader(BPMTDataFile)
        for row in csvReader:
            # Add channel ID : [x (cm), y (cm)]
            BPMTXLocations.update({int(row[0]) : float(row[2]) / 10})
            BPMTYLocations.update({int(row[0]) : float(row[3]) / 10})
    
    return BPMTXLocations, BPMTYLocations

def getPMTPositions(finalChIDs):
    """Replaces the values of channel IDs in an awkward array with their respective X and Y locations."""
    
    BPMTXLocations, BPMTYLocations = getPMTLocationDict()

    # Channel IDs
    keys = ak.Array(BPMTXLocations.keys())
    # Positions [cm]
    xValues = ak.Array([BPMTXLocations[key] for key in keys])
    yValues = ak.Array([BPMTYLocations[key] for key in keys])

    # Flatten awkward array and get its structure
    flattened, num = ak.flatten(finalChIDs), ak.num(finalChIDs)
    # Get indices of where to insert position values
    lookupIndex = np.searchsorted(np.asarray(keys), np.asarray(flattened), side="left")

    # Replace channel IDs with positions and package the arrays back up
    xPos = ak.unflatten(xValues[lookupIndex], num)
    yPos = ak.unflatten(yValues[lookupIndex], num)

    return xPos, yPos

def getS1Mask(data):
    """Return 2D S1 mask usable on awkward arrays"""
    
    s1PulseID = data.get('s1PulseID')
    bottomArea = data.get('bottomArea_phd') # Arbitrary array - only need it for structure
    
    idx = ak.local_index(bottomArea, -1) # Get index structure; compare with S1 pulse IDs 
    s1Mask = idx == s1PulseID # 2D mask for S1 pulses
    
    return s1Mask

def getS2Mask(data):
    """Return 2D S2 mask usable on awkward arrays"""
    
    s2PulseID = data.get('s2PulseID')
    bottomArea = data.get('bottomArea_phd') # Arbitrary array - only need it for structure
    
    idx = ak.local_index(bottomArea, -1) # Get index structure; compare with S2 pulse IDs 
    s2Mask = idx == s2PulseID # 2D mask for S2 pulses
    
    return s2Mask

def getChannelMask(data, bottom = True):
    """Return 2D channel mask of either top or bottom array"""
    
    chIDs = data.get('chID')
    if bottom:
        channelMask1 = ak.where(300 <= chIDs, True, False)
        channelMask2 = ak.where(540 >= chIDs, True, False)
    else:
        channelMask1 = ak.where(0 <= chIDs, True, False)
        channelMask2 = ak.where(252 >= chIDs, True, False)
        
    return channelMask1 * channelMask2

def clusterSize(data, bottom = True):
    """Returns the cluster size of events; requires input data in dictionaries of awkward arrays"""

    if bottom:
        whichArray = 'bottom'
    else:
        whichArray = 'top'
        
    pmtArea = data.get(whichArray + 'Area_phd')
    xCentroid = data.get(whichArray + 'CentroidX_cm')
    yCentroid = data.get(whichArray + 'CentroidY_cm')
    
    s1Mask = getS1Mask(data)
    channelMask = getChannelMask(data, bottom)
    
    chIDs = data.get('chID')
    
    # Create arrays to store x & y locations corresponding to PMT IDs
    finalChIDs = ak.flatten(chIDs[channelMask][s1Mask], axis = 1) # Flatten because dropping all pulses except S1
    xPos, yPos = getPMTPositions(finalChIDs)
    
    channelAreas = data.get('chPulseArea_phd')
    finalAreas = ak.flatten(channelAreas[channelMask][s1Mask], axis = 1) # Flatten because dropping all pulses except S1
    
    summedSize = ak.sum(finalAreas * np.hypot(xPos - ak.flatten(xCentroid[s1Mask]), yPos - ak.flatten(yCentroid[s1Mask])), axis = -1)
    
    clusterSize = ak.to_numpy(summedSize / ak.flatten(pmtArea[s1Mask]))
    
    maxClusterSize = 70
    clusterSize = np.nan_to_num(clusterSize, nan = maxClusterSize, posinf = maxClusterSize, neginf = maxClusterSize)
    
    return clusterSize

def peakFraction(data, bottom = True):
    """Returns peak fraction; requires input data in dictionaries of awkward arrays"""
    chIDs = data.get('chID')

    if bottom:
        whichArray = 'bottom'
    else:
        whichArray = 'top'
        
    pmtArea = data.get(whichArray + 'Area_phd')
    
    # Create S1 Mask 
    s1Mask = getS1Mask(data)
    channelMask = getChannelMask(data, bottom)
    
    channelAreas = data.get('chPulseArea_phd')
    finalAreas = ak.flatten(channelAreas[channelMask][s1Mask], axis = 1) # Flatten because dropping all pulses except S1
    
    result = ak.to_numpy(ak.max(finalAreas, axis = -1) / ak.flatten(pmtArea[s1Mask])).data
    minPeakFraction = 0
    result = np.nan_to_num(result, nan = minPeakFraction, posinf = minPeakFraction, neginf = minPeakFraction)
    
    return result