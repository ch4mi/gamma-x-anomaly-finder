import numpy as np
import awkward as ak
import pandas as pd
import random

from RQConstruct import getS1Mask

from sklearn.ensemble import IsolationForest
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree

def buildTrees(ifInput, nTrees1 = 5000, nTrees2 = 10_000, verbosity = 1):
    """Build the isolation forest and return it"""
    IF = IsolationForest(n_estimators = nTrees1, warm_start = True, verbose = verbosity)
    IF.fit(ifInput)
    IF.set_params(n_estimators = nTrees2) # Refit with new trees
    IF.fit(ifInput)
    return IF

def getAnomalyScore(ifInput, IF):
    anomalyScore = IF.decision_function(ifInput)
    rawAnomalyScore = - anomalyScore - IF.offset_
    return rawAnomalyScore

def insertScoreForMissingS1(data, score):
    """Some events have nS1 != 1. This function inserts null values into the anomaly score array for these events."""
    # Non-single scatters

    s1Mask = getS1Mask(data)
    awkIndices = ak.where(ak.sum(s1Mask, -1) != 1)[0]
    indices = np.sort( ak.to_numpy(awkIndices) )
    
    if len(indices) == 0:
        return score
    else:
        insertIndices = indices - np.arange(len(indices))
        return np.insert(score, insertIndices, -1) #Insert null value of -1

def globalOutlierExplorer(data, 
                          model,
                          batchSize = 3, 
                          pathLengthThreshold = 4, 
                          subSampleSize = 200, 
                          treeSampleSize = 200, 
                          cutWeight = 0.5):
    """This function takes an array of data, an isolation forest model, a maximum path length of an event through a tree in the forest, 
       a sampling size to use on the dataframe, and a geometric cut weight to assign to each depth.
       It returns a dictionary of feature importances."""
    
    # Create a dictionary with the column names
    # columnsDict = {i: RQ for i, RQ in enumerate(['Cluster Size [mm]', 'Peak Fraction Top', 'Peak Fraction Bottom', 'TBA'])}
    columnsDict = {i: RQ for i, RQ in enumerate(['Cluster Size [mm]', 'Peak Fraction Bottom', 'TBA', 'Drift Time [us]'])}

    # Create a list to append first, second, ... cuts
    orderedCutFrequency = []
    
    numEntries = len(data)
    
    if treeSampleSize > len(model):
        raise Exception("Tree sample size greater than number of trees in iForest!")

    if subSampleSize > numEntries:
        raise Exception("Subsampling size greater than number of entries in dataframe!")
        
    # If subsampling size is zero, consider all the samples
    if subSampleSize == 0:
        batchSize = 1
    
    for batch in range(batchSize):
        
        if subSampleSize == 0:
            # Consider all the data
            samples = data
        else:
            # Sample from the full input data; else it'll just take too long
            samples = data[np.random.randint(numEntries, size = subSampleSize)]
        
        # Loop through each sample
        for sample in samples:

            # For each sample, select a random set of trees in the iForest
            randomTrees = random.choices(model, k = treeSampleSize)
            pathLengths = np.zeros(treeSampleSize)

            # Loop through the trees in the iForest
            for treeIndex, treeIF in enumerate(randomTrees):

                nodeFeatures = treeIF.tree_.feature # -2 value corresponds to leaf
                nodeThresholds = treeIF.tree_.threshold

                nodeIndicator = treeIF.decision_path([sample])

                # Index of the leaf that the sample ended up in
                leafID = treeIF.apply([sample])[0]

                pathLengths[treeIndex] = np.size(nodeIndicator)

                # Consider only short paths (these are more likely to have outliers)
                if pathLengths[treeIndex] <= pathLengthThreshold:

                    # Get the node indices of the decision path
                    nodeIndex = nodeIndicator.indices[nodeIndicator.indptr[0]: 
                                                      nodeIndicator.indptr[1]]

                    for i, nodeID in enumerate(nodeIndex):

                        # Terminate if already at the leaf node
                        if leafID == nodeID:
                            continue

                        # Add the feature that was cut and the depth at which it was cut
                        orderedCutFrequency.append([columnsDict.get(nodeFeatures[nodeID]), i+1])
        
        print('Batch %s of %s events processed for %s random trees in the iForest.' % (batch+1, subSampleSize, treeSampleSize))

    orderedCutFrequencyDF = pd.DataFrame(orderedCutFrequency, columns = ['Feature', 'CutNumber'])
 
    # Weigh the features
    featureCountsDict = {RQ : 0 for RQ in columnsDict.values()}
    for RQ, cutNum in orderedCutFrequency:
        featureCountsDict[RQ] += 1 * pow(cutWeight, cutNum - 1)
    
    # Sort the features by importance
    featureCountsDict = {k: v for k, v in sorted(featureCountsDict.items(), key = lambda item: item[1])}
    
    return featureCountsDict

def printFeatureImportances(data, featureCounts):
    """Prints feature information"""
    headers = ['Feature', 'Importance [%]', 'Mean', 'Variance']
    features = list(featureCounts.keys())
    importances = np.array(list(featureCounts.values()))
    percentageImportances = np.round(100 * importances / importances.sum(), 3)
    means = np.round(np.mean(data, axis = 0), 3)
    variances = np.round(np.var(data, axis = 0), 3)
    
    # Order the means and variances
    # featureList = ['Cluster Size [mm]', 'Peak Fraction Top', 'Peak Fraction Bottom', 'TBA'] # Original order
    featureList = ['Cluster Size [mm]', 'Peak Fraction Bottom', 'TBA', 'Drift Time [us]'] # Original order
    meansDict = dict(zip(featureList, means))
    variancesDict = dict(zip(featureList, variances))
    
    orderedMeans = [meansDict.get(feature) for feature in features]
    orderedVariances = [variancesDict.get(feature) for feature in features]
    
    printData = list(map(list, zip(*[features, percentageImportances, orderedMeans, orderedVariances]))) # Transpose
    
    row_format = "{:>20}" * (len(features) + 1)
    print('\n' + row_format.format("", *headers))
    for row in printData[::-1]:
        print(row_format.format(" ", *row))
        
    return