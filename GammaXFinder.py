import IsolationForest
import RQConstruct

from timeit import default_timer as timer
from datetime import timedelta

import sys
import argparse

import uproot
import awkward as ak
import numpy as np

def readData(filename, branches, numEvents = None):
    """Returns dictionary of ROOT data"""

    f = uproot.open(filename)

    branchNames = [b.split(sep = '.')[-1] for b in branches]
    data = dict(zip(branchNames, [f[b].array()[:numEvents] for b in branches]))

    return data

def preprocess(data):
    """Calculate RQs and prepare data to be fed into the isolation forest"""

    clusterSize = RQConstruct.clusterSize(data)
    # pFtop = RQConstruct.peakFraction(data, bottom = False)
    PFbottom = RQConstruct.peakFraction(data, bottom = True)

    tba = data.get('topBottomAsymmetry')
    s1Mask = RQConstruct.getS1Mask(data)
    tbaNumpy = ak.to_numpy(ak.flatten(tba[s1Mask]))

    s1ScattersMask = data['s1PulseID'] != -1 # Need this mask because drift time is pulled from the scatters tree
    driftTime = ak.to_numpy(data.get('driftTime_ns')[s1ScattersMask]) / 1e3 # microseconds

    """
    s2Mask = RQConstruct.getS2Mask(data)
    s2x = ak.flatten( data['s2Xposition_cm'][s2Mask] )
    s2y = ak.flatten( data['s2Yposition_cm'][s2Mask] )
    radius = ak.to_numpy( np.hypot(s2x, s2y) )
    """

    # dataList = [clusterSize, pFtop, pFbottom, tbaNumpy]
    dataList = [clusterSize, PFbottom, tbaNumpy, driftTime] #, radius]
    if len(set([len(l) for l in dataList])) != 1:
        for col in dataList:
            print(str(len(col)))
        raise Exception("Error in preprocessing. Data have different lengths.")

    ifInput = np.stack(dataList).T

    return ifInput

def getCommonBins(data, nBins, upperLim = np.inf, lowerLim = 0):
    """Return a common binning for plotting multiple histograms on the same plot.
    'data' is a tuple of data, and the limits restrict all of them to a common range."""
    restrictedData = []
    for array in data:
        restrictedData.append( array[(array >= lowerLim) & (array <= upperLim)] )
    return np.histogram(np.hstack(tuple(restrictedData)), bins = nBins)[1]

def writeToTextFile(filename, completeScores, targetFile):
    """Write event identifying info and anomaly scores into a text file"""

    branches = ['Events/eventHeader./eventHeader.runID',
                'Events/eventHeader./eventHeader.eventID']

    eventData = readData(filename, branches)

    for key, dataList in eventData.items():
        if len(dataList) != len(completeScores):
            raise Exception("ERROR: " + key + " has wrong length!")

    runIDs = ak.to_numpy(eventData.get('runID'))
    eventIDs = ak.to_numpy(eventData.get('eventID'))

    dataToWrite = np.column_stack((runIDs, eventIDs, completeScores))

    np.savetxt(targetFile, dataToWrite, fmt = ['% 4d', '% 4d', '%4.3f'], delimiter = '\t', header = 'Run ID  Event ID  GX Anomaly Score')
    print('Data written to ' + targetFile)

    return

def makePlots(scores):
    """Create relevant plots"""
    import matplotlib.pyplot as plt

    plt.hist(scores, bins = 50)
    plt.xlim(0)
    plt.ylabel('Counts')
    plt.xlabel('Raw Anomaly Score')
    plt.savefig('anomalyScores.png')

    return

def main():

    # ARGUMENTS
    parser = argparse.ArgumentParser()
    parser.add_argument("-O", "--outputType", help = 'Choose output type: text (t) or ROOT (r)', default = 't')
    parser.add_argument("-F", "--featureImportance", help = 'Print feature importances', action = "store_true", default = False)
    parser.add_argument("-P", "--plots", help = 'Record plots', action = "store_true", default = False)

    args = parser.parse_args()

    # READING
    readingBranches = ['Events/pulsesTPC./pulsesTPC.pulseID',
                       'Events/pulsesTPC./pulsesTPC.bottomArea_phd',
                       'Events/pulsesTPC./pulsesTPC.topArea_phd',
                       'Events/pulsesTPC./pulsesTPC.topBottomAsymmetry',
                       'Scatters/ss./ss.s1PulseID',
                       'Scatters/ss./ss.driftTime_ns',
                       'Events/pulsesTPC./pulsesTPC.bottomCentroidX_cm',
                       'Events/pulsesTPC./pulsesTPC.bottomCentroidY_cm',
                       'Events/pulsesTPC./pulsesTPC.topCentroidX_cm',
                       'Events/pulsesTPC./pulsesTPC.topCentroidY_cm',
                       'Events/pulsesTPC./pulsesTPC.chID',
                       'Events/pulsesTPC./pulsesTPC.chPulseArea_phd']

    filename = '/global/cfs/cdirs/lz/data/reconstructed/commissioning/LZAP-5.3.4_PROD-1/202110/lz_202110150821_005665/rq/lz_005665_000000_rq.root'
    print("\nReading in ROOT data...")
    data = readData(filename, readingBranches)
    print("\nAll branches read. Starting anomaly score calculation...")
    ifInput = preprocess(data)

    # ISOLATION FOREST CALCULATION
    print("\nBuilding trees...")
    IF = IsolationForest.buildTrees(ifInput)
    rawScores = IsolationForest.getAnomalyScore(ifInput, IF)
    completeScores = IsolationForest.insertScoreForMissingS1(data, rawScores)

    # WRITING
    if args.outputType == 't':
        print('\nWriting text file...')
        writeToTextFile(filename, completeScores, "anomalyResults.txt")
    else:
        print("\nOnly text output mode implement. Exiting!")

    if args.featureImportance:
        print("\nGetting feature importances...")
        featureCounts = IsolationForest.globalOutlierExplorer(ifInput, IF)
        IsolationForest.printFeatureImportances(ifInput, featureCounts)

    if args.plots:
        print("\nCreating plots...")
        makePlots(completeScores)

    return

if __name__ == '__main__':
    print("\nRunning anomaly finder...")
    start = timer()
    main()
    end = timer()
    print("\n Running time:")
    print(timedelta( seconds = end - start ))
